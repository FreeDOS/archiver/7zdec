# 7zdec

Unpacks all 7z files. Supports long file names, 2 gigabyte dictionaries, and all filters.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## 7ZDEC.LSM

<table>
<tr><td>title</td><td>7zdec</td></tr>
<tr><td>version</td><td>24.08</td></tr>
<tr><td>entered nbsp;date</td><td>2017-11-13</td></tr>
<tr><td>description</td><td>Standalone decompressor for 7-Zip archives</td></tr>
<tr><td>keywords</td><td>7z, 7zdec, 7zdecode, archivers, decompress, extract</td></tr>
<tr><td>author</td><td>Igor Pavlov</td></tr>
<tr><td>maintained nbsp;by</td><td>dajhorn@gmail.com (Darik Horn)</td></tr>
<tr><td>alternate nbsp;site</td><td>https://gitlab.com/FreeDOS/archiver/7zdec</td></tr>
<tr><td>primary nbsp;site</td><td>https://sourceforge.net/projects/sevenzip</td></tr>
<tr><td>original nbsp;site</td><td>https://www.7-zip.org</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying nbsp;policy</td><td>Public Domain</td></tr>
<tr><td>summary</td><td>Unpacks all 7z files. Supports long file names, 2 gigabyte dictionaries, and all filters.</td></tr>
</table>
